hi clear
syntax reset
let g:colors_name = "OrangeMesa"

" color declarations
let cblack = "#050505"
let cred = "#963200"
let cgreen = "#323232"
let cyellow = "#af550a"
let cblue = "#6482a0"
let cmagenta = "#644b32"
let ccyan = "#5f8c87"
let cwhite = "#7d7d7d"
let clightblack = "#969696"
let clightred = "#ff7832"
let clightgreen = "#afafaf"
let clightyellow = "#ff964b"
let clightblue = "#81a2be"
let clightmagenta = "#c89678"
let clightcyan = "#8abeb7"
let clightwhite = "#ffffff"



" gui colors
exe 'hi Cursor gui=NONE guibg='.clightwhite.' cterm=NONE ctermbg=white'
exe 'hi CursorLine gui=NONE guibg='.cmagenta.' cterm=NONE ctermbg=darkgrey'
exe 'hi CursorLineNr gui=NONE guifg='.clightred.' cterm=NONE ctermfg=blue'
exe 'hi FoldColumn gui=NONE guifg='.cblue.' guibg='.cblack.' cterm=NONE ctermfg=darkblue ctermbg=black'
exe 'hi Folded gui=NONE guifg='.clightblack.' guibg='.cblack.' cterm=NONE ctermbg=black ctermfg=darkgrey'
exe 'hi LineNr gui=NONE guifg='.cgreen.' cterm=NONE ctermfg=darkgrey'
exe 'hi Normal gui=NONE guibg='.cblack.' guifg='.cwhite.' cterm=NONE ctermbg=black ctermfg=grey'
exe 'hi NonText gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=darkgrey '
exe 'hi SignColumn gui=NONE guifg='.cblue.' guibg='.cblack.' cterm=NONE ctermfg=darkblue ctermbg=black'
exe 'hi StatusLine gui=bold guifg='.cblack.' guibg='.clightred.' cterm=NONE ctermbg=grey ctermfg=black'
exe 'hi StatusLineNC gui=NONE guibg='.cgreen.' guifg='.cwhite.' cterm=NONE ctermbg=darkgrey ctermfg=grey'
exe 'hi VertSplit gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'
exe 'hi Visual gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'

" misc features
exe 'hi ErrorMsg gui=NONE guibg='.cblack.' guifg='.clightred.' cterm=NONE ctermbg=black ctermfg=red'
exe 'hi IncSearch gui=NONE guibg='.cgreen.' cterm=NONE ctermbg=darkgreen'
exe 'hi MatchParen gui=bold guifg='.clightgreen.' cterm=NONE ctermfg=green'
exe 'hi PMenu gui=NONE guibg='.cblue.' guifg='.clightwhite.' cterm=NONE ctermbg=darkblue ctermfg=white'
exe 'hi PMenuSel gui=NONE guibg='.clightblue.' guifg='.clightwhite.' cterm=NONE ctermbg=blue ctermfg=white'
exe 'hi Search gui=NONE guibg='.cgreen.' cterm=NONE ctermbg=darkgreen'
exe 'hi WarningMsg gui=NONE guifg='.clightyellow.' cterm=NONE ctermfg=yellow'

" code syntax
exe 'hi Comment gui=NONE guifg='.cgreen.' cterm=NONE ctermfg=darkgreen'
"exe 'hi Constant gui=NONE guifg='.cmagenta.' cterm=NONE ctermfg=darkmagenta'
exe 'hi Constant gui=NONE guifg='.cred.' cterm=NONE ctermfg=darkmagenta'
exe 'hi Error gui=NONE guifg='.clightred.' guibg='.cblack.' cterm=NONE ctermbg=black ctermfg=red'
exe 'hi Identifier gui=NONE guifg='.clightmagenta.' cterm=NONE ctermfg=magenta'
exe 'hi PreProc gui=NONE guifg='.clightgreen.' cterm=NONE ctermfg=green'
exe 'hi Statement gui=NONE guifg='.clightwhite.' cterm=NONE ctermfg=white'
exe 'hi String gui=NONE guifg='.clightyellow.' cterm=NONE ctermfg=cyan'
exe 'hi Todo gui=NONE guifg='.cblack.' guibg='.clightred.' cterm=NONE ctermbg=red ctermfg=black'
"exe 'hi Type gui=NONE guifg='.ccyan.' cterm=NONE ctermfg=darkcyan'
exe 'hi Type gui=NONE guifg='.clightred.' cterm=NONE ctermfg=blue'
exe 'hi Ignore gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'

" I don't even know....
exe 'hi Title gui=NONE guifg='.clightmagenta.' cterm=NONE ctermfg=magenta'
exe 'hi Directory gui=bold guifg='.ccyan.' cterm=none ctermfg=darkcyan'

pushd hkcu:/console
$0 = '%systemroot%_system32_windowspowershell_v1.0_powershell.exe'
ni $0 -f
sp $0 ColorTable00 0x00050505
sp $0 ColorTable01 0x00a08264
sp $0 ColorTable02 0x00323232
sp $0 ColorTable03 0x00878c5f
sp $0 ColorTable04 0x00003296
sp $0 ColorTable05 0x00324b64
sp $0 ColorTable06 0x000a55af
sp $0 ColorTable07 0x007d7d7d
sp $0 ColorTable08 0x00969696
sp $0 ColorTable09 0x00bea281
sp $0 ColorTable10 0x00afafaf
sp $0 ColorTable11 0x00b7be8a
sp $0 ColorTable12 0x003278ff
sp $0 ColorTable13 0x007896c8
sp $0 ColorTable14 0x004b96ff
sp $0 ColorTable15 0x00ffffff
popd

pushd hkcu:/console
$0 = '%systemroot%_system32_windowspowershell_v1.0_powershell.exe'
ni $0 -f
sp $0 ColorTable00 0x00000000
sp $0 ColorTable01 0x00646464
sp $0 ColorTable02 0x00323232
sp $0 ColorTable03 0x00888888
sp $0 ColorTable04 0x001e1e1e
sp $0 ColorTable05 0x00969696
sp $0 ColorTable06 0x004b4b4b
sp $0 ColorTable07 0x00c8c8c8
sp $0 ColorTable08 0x00555555
sp $0 ColorTable09 0x00ffaf32
sp $0 ColorTable10 0x0096ff32
sp $0 ColorTable11 0x00ffff19
sp $0 ColorTable12 0x003232ff
sp $0 ColorTable13 0x00ff32af
sp $0 ColorTable14 0x007dffff
sp $0 ColorTable15 0x00ffffff
popd

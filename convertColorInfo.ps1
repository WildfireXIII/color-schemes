#*************************************************************
#  File: convertColorInfo.ps1
#  Date created: 1/11/2016
#  Date edited: 1/11/2016
#  Author: Nathan Martindale
#  Copyright � 2016 Digital Warrior Labs
#  Description: Script that takes common color theme syntax and 
#    converts into the colors that ps1 takes for registry
#*************************************************************

param (
	[string]$theme = "",
	[string]$out = "colorOutput.txt",
	[string]$style = "ps1"
)

$colors = @{}

# read in the file
$lines = Get-Content $theme
foreach ($line in $lines)
{
	$key = $line.Substring(0, $line.IndexOf("="))
	$value = $line.Substring($line.IndexOf("=")+1)

	if ($style -eq "ps1") 
	{
		$r = $value.Substring(0,2)
		$g = $value.Substring(2,2)
		$b = $value.Substring(4)
		
		$colors.Add($key, "$b$g$r")
	}
	else { $colors.Add($key,$value) }
}

if ($style -eq "ps1")
{
	echo "pushd hkcu:/console" >> $out
	echo "`$0 = '%systemroot%_system32_windowspowershell_v1.0_powershell.exe'" >> $out
	echo "ni `$0 -f" >> $out
	
	echo "sp `$0 ColorTable00 0x00$($colors['black'])" >> $out
	echo "sp `$0 ColorTable01 0x00$($colors['blue'])" >> $out
	echo "sp `$0 ColorTable02 0x00$($colors['green'])" >> $out
	echo "sp `$0 ColorTable03 0x00$($colors['cyan'])" >> $out
	echo "sp `$0 ColorTable04 0x00$($colors['red'])" >> $out
	echo "sp `$0 ColorTable05 0x00$($colors['magenta'])" >> $out
	echo "sp `$0 ColorTable06 0x00$($colors['yellow'])" >> $out
	echo "sp `$0 ColorTable07 0x00$($colors['white'])" >> $out
	echo "sp `$0 ColorTable08 0x00$($colors['lightblack'])" >> $out
	echo "sp `$0 ColorTable09 0x00$($colors['lightblue'])" >> $out
	echo "sp `$0 ColorTable10 0x00$($colors['lightgreen'])" >> $out
	echo "sp `$0 ColorTable11 0x00$($colors['lightcyan'])" >> $out
	echo "sp `$0 ColorTable12 0x00$($colors['lightred'])" >> $out
	echo "sp `$0 ColorTable13 0x00$($colors['lightmagenta'])" >> $out
	echo "sp `$0 ColorTable14 0x00$($colors['lightyellow'])" >> $out
	echo "sp `$0 ColorTable15 0x00$($colors['lightwhite'])" >> $out

	echo "popd" >> $out
}

if ($style -eq 'sh')
{
	echo "echo -e `"" >> $out
	echo "`t\e]P0$($colors['black'])" >> $out
	echo "`t\e]P1$($colors['red'])" >> $out
	echo "`t\e]P2$($colors['green'])" >> $out
	echo "`t\e]P3$($colors['yellow'])" >> $out
	echo "`t\e]P4$($colors['blue'])" >> $out
	echo "`t\e]P5$($colors['magenta'])" >> $out
	echo "`t\e]P6$($colors['cyan'])" >> $out
	echo "`t\e]P7$($colors['white'])" >> $out
	echo "`t\e]P8$($colors['lightblack'])" >> $out
	echo "`t\e]P9$($colors['lightred'])" >> $out
	echo "`t\e]PA$($colors['lightgreen'])" >> $out
	echo "`t\e]PB$($colors['lightyellow'])" >> $out
	echo "`t\e]PC$($colors['lightblue'])" >> $out
	echo "`t\e]PD$($colors['lightmagenta'])" >> $out
	echo "`t\e]PE$($colors['lightcyan'])" >> $out
	echo "`t\e]PF$($colors['lightwhite'])" >> $out
	echo "`"" >> $out
}

if ($style -eq 'vim')
{
	echo "let cblack = `"#$($colors['black'])`"" >> $out
	echo "let cred = `"#$($colors['red'])`"" >> $out
	echo "let cgreen = `"#$($colors['green'])`"" >> $out
	echo "let cyellow = `"#$($colors['yellow'])`"" >> $out
	echo "let cblue = `"#$($colors['blue'])`"" >> $out
	echo "let cmagenta = `"#$($colors['magenta'])`"" >> $out
	echo "let ccyan = `"#$($colors['cyan'])`"" >> $out
	echo "let cwhite = `"#$($colors['white'])`"" >> $out
	echo "let clightblack = `"#$($colors['lightblack'])`"" >> $out
	echo "let clightred = `"#$($colors['lightred'])`"" >> $out
	echo "let clightgreen = `"#$($colors['lightgreen'])`"" >> $out
	echo "let clightyellow = `"#$($colors['lightyellow'])`"" >> $out
	echo "let clightblue = `"#$($colors['lightblue'])`"" >> $out
	echo "let clightmagenta = `"#$($colors['lightmagenta'])`"" >> $out
	echo "let clightcyan = `"#$($colors['lightcyan'])`"" >> $out
	echo "let clightwhite = `"#$($colors['lightwhite'])`"" >> $out
}

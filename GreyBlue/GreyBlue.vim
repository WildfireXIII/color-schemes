hi clear
syntax reset
let g:colors_name = "GreyBlue"

" color declarations
let cblack = "#091114"
let cred = "#803030"
let cgreen = "#3b645a"
let cyellow = "#786940"
let cblue = "#325064"
let cmagenta = "#826496"
let ccyan = "#327878"
let cwhite = "#78afb4"
let clightblack = "#28343c"
let clightred = "#c95d5d"
let clightgreen = "#78c8a0"
let clightyellow = "#dcc864"
let clightblue = "#50aadc"
let clightmagenta = "#b48cfa"
let clightcyan = "#64dcfa"
let clightwhite = "#ffffff"

" gui colors
exe 'hi Cursor gui=NONE guibg='.clightwhite.' cterm=NONE ctermbg=white'
exe 'hi CursorLine gui=NONE guibg='.clightblack.' cterm=NONE ctermbg=darkgrey'
exe 'hi CursorLineNr gui=NONE guifg='.clightblue.' cterm=NONE ctermfg=blue'
exe 'hi FoldColumn gui=NONE guifg='.cblue.' guibg='.cblack.' cterm=NONE ctermfg=darkblue ctermbg=black'
exe 'hi Folded gui=NONE guifg='.clightblack.' guibg='.cblack.' cterm=NONE ctermbg=black ctermfg=darkgrey'
exe 'hi LineNr gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=darkgrey'
exe 'hi Normal gui=NONE guibg='.cblack.' guifg='.cwhite.' cterm=NONE ctermbg=black ctermfg=grey'
exe 'hi NonText gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=darkgrey '
exe 'hi SignColumn gui=NONE guifg='.cblue.' guibg='.cblack.' cterm=NONE ctermfg=darkblue ctermbg=black'
"exe 'hi StatusLine gui=bold guifg='.clightwhite.' guibg='.ccyan.' cterm=NONE ctermbg=cyan ctermfg=white '
exe 'hi StatusLine gui=bold guifg='.cblack.' guibg='.cwhite.' cterm=NONE ctermbg=grey ctermfg=black'
exe 'hi StatusLineNC gui=NONE guibg='.clightblack.' guifg='.cwhite.' cterm=NONE ctermbg=darkgrey ctermfg=grey'
exe 'hi VertSplit gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'
exe 'hi Visual gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'

" misc features
exe 'hi ErrorMsg gui=NONE guibg='.cblack.' guifg='.clightred.' cterm=NONE ctermbg=black ctermfg=red'
exe 'hi IncSearch gui=NONE guibg='.cgreen.' cterm=NONE ctermbg=darkgreen'
exe 'hi MatchParen gui=bold guifg='.clightgreen.' cterm=NONE ctermfg=green'
exe 'hi PMenu gui=NONE guibg='.cblue.' guifg='.clightwhite.' cterm=NONE ctermbg=darkblue ctermfg=white'
exe 'hi PMenuSel gui=NONE guibg='.clightblue.' guifg='.clightwhite.' cterm=NONE ctermbg=blue ctermfg=white'
exe 'hi Search gui=NONE guibg='.cgreen.' cterm=NONE ctermbg=darkgreen'
exe 'hi WarningMsg gui=NONE guifg='.clightyellow.' cterm=NONE ctermfg=yellow'

" code syntax
exe 'hi Comment gui=NONE guifg='.cgreen.' cterm=NONE ctermfg=darkgreen'
exe 'hi Constant gui=NONE guifg='.cmagenta.' cterm=NONE ctermfg=darkmagenta'
exe 'hi Error gui=NONE guifg='.clightred.' guibg='.cblack.' cterm=NONE ctermbg=black ctermfg=red'
exe 'hi Identifier gui=NONE guifg='.clightmagenta.' cterm=NONE ctermfg=magenta'
exe 'hi PreProc gui=NONE guifg='.clightgreen.' cterm=NONE ctermfg=green'
exe 'hi Statement gui=NONE guifg='.clightwhite.' cterm=NONE ctermfg=white'
exe 'hi String gui=NONE guifg='.clightcyan.' cterm=NONE ctermfg=cyan'
exe 'hi Todo gui=NONE guifg='.cblack.' guibg='.clightred.' cterm=NONE ctermbg=red ctermfg=black'
"exe 'hi Type gui=NONE guifg='.ccyan.' cterm=NONE ctermfg=darkcyan'
exe 'hi Type gui=NONE guifg='.clightblue.' cterm=NONE ctermfg=blue'
exe 'hi Ignore gui=NONE guifg='.clightblack.' cterm=NONE ctermfg=grey'

" I don't even know....
exe 'hi Title gui=NONE guifg='.clightmagenta.' cterm=NONE ctermfg=magenta'
exe 'hi Directory gui=bold guifg='.ccyan.' cterm=none ctermfg=darkcyan'

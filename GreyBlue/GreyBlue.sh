#!/bin/sh
if [ "$TERM" = "linux" ]; then
	/bin/echo -e "
	\e]P0091114
	\e]P1803030
	\e]P23b645a
	\e]P3786940
	\e]P4325064
	\e]P5826496
	\e]P6327878
	\e]P778afb4
	\e]P828343c
	\e]P9c95d5d
	\e]PA78c8a0
	\e]PBdcc864
	\e]PC50aadc
	\e]PDb48cfa
	\e]PE64dcfa
	\e]PFffffff
	"

	# get rid of artifiacts
	clear
fi
